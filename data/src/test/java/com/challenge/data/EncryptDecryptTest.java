package com.challenge.data;

import android.util.Base64;

import com.challenge.core.encryptdecrypt.EncryptDecrypt;

import org.junit.Before;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKey;

import static org.junit.Assert.assertEquals;

public class EncryptDecryptTest {

    private EncryptDecrypt encryptDecrypt;
    private byte[] encryptionKey;
    private SecretKey secretKey;

    @Before
    public void init() {
        try {
        encryptDecrypt = new EncryptDecrypt();
        SecureRandom random = new SecureRandom();
        byte[] encryptionKey = new byte[16];
        random.nextBytes(encryptionKey);
            secretKey = encryptDecrypt.generateKey(encryptionKey);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_encrypt_and_decrypt() {
        try {
            byte[] encrypted = encryptDecrypt.encrypt(secretKey, "hello_world");
            String decrypted = encryptDecrypt.decrypt(secretKey, encrypted);
            assertEquals("hello_world", decrypted);
        } catch (Exception e) {

        }
    }

    @Test
    public void test_convert_encrypted_to_string_and_convert_string_to_byte_and_decrypt() {
        try {
            byte[] encrypted = encryptDecrypt.encrypt(secretKey, "hello_world");
            String strEncrypted = Base64.encodeToString(encrypted, Base64.DEFAULT);
            byte[] byteEncrypted = Base64.decode(strEncrypted, Base64.DEFAULT);
            String decrypted = encryptDecrypt.decrypt(secretKey, byteEncrypted);
            assertEquals("hello_world", decrypted);
        } catch (Exception e) {

        }
    }
}
