package com.challenge.data.api;

import com.challenge.core.model.TimezoneAppgate;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TimezoneService {
    @GET("timezoneJSON")
    Call<TimezoneAppgate> getTimezone(@Query("formatted") boolean formated,
                                      @Query("lat") double lat,
                                      @Query("lng") double lng,
                                      @Query("username") String username,
                                      @Query("style") String full);
}
