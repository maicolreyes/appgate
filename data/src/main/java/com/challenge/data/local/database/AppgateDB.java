package com.challenge.data.local.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.challenge.data.local.database.dao.UserDao;
import com.challenge.data.local.database.dao.UserValidationDao;
import com.challenge.data.model.User;
import com.challenge.data.model.UserValidation;

@Database(entities = {User.class, UserValidation.class},version = 1)
public abstract class AppgateDB extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract UserValidationDao userValidationDao();
}
