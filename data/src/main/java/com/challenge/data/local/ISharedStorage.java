package com.challenge.data.local;

public interface ISharedStorage {
    void setString(String key, String value);
    String getString(String key);
}
