package com.challenge.data.local.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.challenge.data.model.User;

@Dao
public interface UserDao {
    @Insert
    void insertUser(User user);

    @Query("SELECT COUNT(*) FROM user WHERE username = :userName")
    LiveData<Integer> isUser(String userName);

    @Query("SELECT * FROM user WHERE username = :userName")
    LiveData<User> getUser(String userName);
}
