package com.challenge.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ApplicationContext;

public class SharedStorage implements ISharedStorage {

    private SharedPreferences sharedPreferences;

    @Inject
    public SharedStorage(@ApplicationContext Context context) {
        this.sharedPreferences = context.getSharedPreferences("appgate", Context.MODE_PRIVATE);
    }

    @Override
    public void setString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    @Override
    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }
}
