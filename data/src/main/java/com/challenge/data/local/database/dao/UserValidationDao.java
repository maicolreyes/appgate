package com.challenge.data.local.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.challenge.data.model.User;
import com.challenge.data.model.UserValidation;

import java.util.List;

@Dao
public interface UserValidationDao {
    @Insert
    void insertUserValidation(UserValidation userValidation);

    @Query("SELECT * FROM user_validation WHERE idUser = :idUser")
    LiveData<List<UserValidation>> getUserValidations(int idUser);
}
