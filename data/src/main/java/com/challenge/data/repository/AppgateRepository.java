package com.challenge.data.repository;

import androidx.lifecycle.LiveData;

import com.challenge.core.model.TimezoneAppgate;
import com.challenge.data.local.database.dao.UserDao;
import com.challenge.data.local.database.dao.UserValidationDao;
import com.challenge.data.model.User;
import com.challenge.data.model.UserValidation;
import com.challenge.data.remote.NetworkCall;
import com.challenge.data.remote.Resource;
import com.challenge.data.remote.TimezoneRemoteDataSource;

import java.util.List;

import javax.inject.Inject;

public class AppgateRepository {
    private TimezoneRemoteDataSource remoteDataSource;
    private UserDao userDao;
    private UserValidationDao userValidationDao;

    @Inject
    public AppgateRepository(TimezoneRemoteDataSource remoteDataSource, UserDao userDao, UserValidationDao userValidationDao) {
        this.remoteDataSource = remoteDataSource;
        this.userDao = userDao;
        this.userValidationDao = userValidationDao;
    }

    public LiveData<Resource<TimezoneAppgate>> getTime(double lat, double lng) {
        return new NetworkCall<TimezoneAppgate>().makeCall(remoteDataSource.getTimeZone(lat, lng));
    }

    public LiveData<Integer> isUser(String username) {
        return userDao.isUser(username);
    }

    public void saveUser(User user) {
        userDao.insertUser(user);
    }

    public LiveData<User> getUser(String username) {
        return userDao.getUser(username);
    }

    public void saveUserValidation(UserValidation userValidation) {
        userValidationDao.insertUserValidation(userValidation);
    }

    public LiveData<List<UserValidation>> getUserValidations(int idUser) {
        return userValidationDao.getUserValidations(idUser);
    }
}
