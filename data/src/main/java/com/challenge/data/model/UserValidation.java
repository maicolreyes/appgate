package com.challenge.data.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user_validation")
public class UserValidation {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int idUser;
    private boolean isValid;
    private String date;

    public UserValidation(int idUser, boolean isValid, String date) {
        this.idUser = idUser;
        this.isValid = isValid;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String isValidIntent() {
        if (isValid) {
            return "Exitoso";
        } else {
            return "Fallido";
        }
    }
}
