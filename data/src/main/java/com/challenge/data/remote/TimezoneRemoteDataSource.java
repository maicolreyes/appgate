package com.challenge.data.remote;


import com.challenge.core.model.TimezoneAppgate;
import com.challenge.data.api.TimezoneService;

import javax.inject.Inject;

import retrofit2.Call;

public class TimezoneRemoteDataSource {
    private TimezoneService service;

    @Inject
    public TimezoneRemoteDataSource(TimezoneService service){
        this.service = service;
    }
    public Call<TimezoneAppgate> getTimeZone(double lat, double lng) {
       return service.getTimezone(true, lat, lng, "qa_mobile_easy", "full");
    }
}
