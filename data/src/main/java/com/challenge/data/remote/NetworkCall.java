package com.challenge.data.remote;

import androidx.lifecycle.MutableLiveData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkCall<T> {
    private Call<T> call;
    public MutableLiveData<Resource<T>> makeCall(Call<T> call) {
        this.call = call;
        CallBackOwn<T> callback = new CallBackOwn<T>();
        callback.result.postValue(Resource.loading(null));
        this.call.clone().enqueue(callback);
        return callback.result;
    }

    class CallBackOwn<T> implements Callback<T> {

        public MutableLiveData<Resource<T>> result = new MutableLiveData();

        @Override
        public void onResponse(Call<T> call, Response<T> response) {
            if(response.isSuccessful()) {
                result.postValue(Resource.success(response.body()));
            } else{
                result.postValue(Resource.error(response.message(), null));
            }
        }

        @Override
        public void onFailure(Call<T> call, Throwable t) {
            result.postValue(Resource.error(t.getMessage(), null));
            t.printStackTrace();
        }
    }
}
