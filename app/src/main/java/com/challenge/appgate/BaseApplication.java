package com.challenge.appgate;

import android.app.Application;

import com.facebook.flipper.android.AndroidFlipperClient;
import com.facebook.flipper.android.utils.FlipperUtils;
import com.facebook.flipper.core.FlipperClient;
import com.facebook.flipper.plugins.databases.DatabasesFlipperPlugin;
import com.facebook.flipper.plugins.inspector.DescriptorMapping;
import com.facebook.flipper.plugins.inspector.InspectorFlipperPlugin;
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin;
import com.facebook.soloader.SoLoader;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            SoLoader.init(this, false);
            if (FlipperUtils.shouldEnableFlipper(this)) {
                FlipperClient client = AndroidFlipperClient.getInstance(this);
                NetworkFlipperPlugin networkFlipperPlugin = new NetworkFlipperPlugin();
                client.addPlugin(new InspectorFlipperPlugin(this, DescriptorMapping.withDefaults()));
                client.addPlugin(new DatabasesFlipperPlugin(this));
                client.addPlugin(networkFlipperPlugin);
                client.start();
            }
        }
    }
}
