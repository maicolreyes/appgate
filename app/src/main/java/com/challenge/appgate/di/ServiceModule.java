package com.challenge.appgate.di;

import com.challenge.data.api.TimezoneService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import retrofit2.Retrofit;

@Module
@InstallIn(ApplicationComponent.class)
public class ServiceModule {
    @Provides
    @Singleton
    public static TimezoneService provideTimezoneService(Retrofit retrofit) {
        return retrofit.create(TimezoneService.class);
    }
}
