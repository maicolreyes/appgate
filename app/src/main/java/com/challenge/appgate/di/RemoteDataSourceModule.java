package com.challenge.appgate.di;

import com.challenge.data.api.TimezoneService;
import com.challenge.data.remote.TimezoneRemoteDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;

@Module
@InstallIn(ApplicationComponent.class)
public class RemoteDataSourceModule {
    @Provides
    @Singleton
    TimezoneRemoteDataSource provideTimezonRemoteDataSource(TimezoneService service) {
        return new TimezoneRemoteDataSource(service);
    }
}
