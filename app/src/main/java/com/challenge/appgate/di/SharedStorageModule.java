package com.challenge.appgate.di;
import com.challenge.data.local.ISharedStorage;
import com.challenge.data.local.SharedStorage;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;

@Module
@InstallIn(ApplicationComponent.class)
abstract class SharedStorageModule {
    @Singleton
    @Binds
    abstract ISharedStorage provideStorage(SharedStorage sharedStorage);
}
