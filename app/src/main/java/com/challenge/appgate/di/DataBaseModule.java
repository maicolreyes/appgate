package com.challenge.appgate.di;

import android.app.Application;

import androidx.room.Room;
import com.challenge.data.local.database.AppgateDB;
import com.challenge.data.local.database.dao.UserDao;
import com.challenge.data.local.database.dao.UserValidationDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;

@Module
@InstallIn(ApplicationComponent.class)
public class DataBaseModule {
    @Provides
    @Singleton
    public static AppgateDB provideAppgateDB(Application application){
        return Room.databaseBuilder(application,AppgateDB.class,"Appgate")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
    }

    @Provides
    @Singleton
    public static UserDao provideUserDao(AppgateDB appgateDB){
        return appgateDB.userDao();
    }

    @Provides
    @Singleton
    public static UserValidationDao provideUserValidationDao(AppgateDB appgateDB){
        return appgateDB.userValidationDao();
    }
}
