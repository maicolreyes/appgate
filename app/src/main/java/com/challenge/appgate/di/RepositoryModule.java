package com.challenge.appgate.di;

import com.challenge.data.local.database.dao.UserDao;
import com.challenge.data.local.database.dao.UserValidationDao;
import com.challenge.data.remote.TimezoneRemoteDataSource;
import com.challenge.data.repository.AppgateRepository;

import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;

@Module
@InstallIn(ApplicationComponent.class)
public class RepositoryModule {
    AppgateRepository provideRepository(TimezoneRemoteDataSource remoteDataSource, UserDao userDao, UserValidationDao userValidationDao) {
        return new AppgateRepository(remoteDataSource, userDao, userValidationDao);
    }
}
