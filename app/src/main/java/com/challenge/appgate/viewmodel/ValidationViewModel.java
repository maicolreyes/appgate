package com.challenge.appgate.viewmodel;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.challenge.data.local.ISharedStorage;
import com.challenge.data.model.User;
import com.challenge.data.model.UserValidation;
import com.challenge.data.repository.AppgateRepository;

import java.util.List;

public class ValidationViewModel extends ViewModel {
    private AppgateRepository repository;

    @ViewModelInject
    public ValidationViewModel(AppgateRepository repository) {
        this.repository = repository;
    }

    public MutableLiveData<List<UserValidation>> userValidations = new MutableLiveData<>();
    public Observer<List<UserValidation>> userObserver = response -> {
        proccessUserResponse(response);
    };

    public void callGetUserValidations(int idUser) {
        repository.getUserValidations(idUser).observeForever(user -> {
            userObserver.onChanged(user);
        });
    }

    public LiveData<List<UserValidation>> getUserValidations() {
        return Transformations.map(userValidations, input -> input);
    }

    private void proccessUserResponse(List<UserValidation> response) {
        userValidations.postValue(response);
    }
}
