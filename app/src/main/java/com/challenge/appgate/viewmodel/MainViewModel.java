package com.challenge.appgate.viewmodel;

import android.util.Base64;

import androidx.arch.core.util.Function;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.challenge.core.ConstantsAppgate;
import com.challenge.core.encryptdecrypt.EncryptDecrypt;
import com.challenge.core.model.TimezoneAppgate;
import com.challenge.data.local.ISharedStorage;
import com.challenge.data.model.User;
import com.challenge.data.model.UserValidation;
import com.challenge.data.remote.Resource;
import com.challenge.data.repository.AppgateRepository;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class MainViewModel extends ViewModel {
    private AppgateRepository repository;
    private ISharedStorage sharedStorage;

    @ViewModelInject
    public MainViewModel(AppgateRepository repository, ISharedStorage sharedStorage) {
        this.repository = repository;
        this.sharedStorage = sharedStorage;
    }

    public LiveData<Integer> callIsUser(String username) {
       return repository.isUser(username);
    }

    public MutableLiveData<User> user = new MutableLiveData<>();
    public Observer<User> userObserver = userResponse -> {
        proccessUserResponse(userResponse);
    };

    public void callGetUser(String username) {
        repository.getUser(username).observeForever(user -> {
            userObserver.onChanged(user);
        });
    }

    public LiveData<User> getUser() {
        return Transformations.map(user, input -> input);
    }

    private void proccessUserResponse(User response) {
        user.postValue(response);
    }

    public void saveUser(User user) {
        repository.saveUser(user);
    }

    public void saveUserValidation(UserValidation userValidation) {
        repository.saveUserValidation(userValidation);
    }

    public boolean isPassword(String password) {
        final String regex = "^(?=.*[A-Za-z])(?=.*\\d)(?=.+[$@%*#&:;.,_¡¿(<=>)?!])[A-Za-z\\d$@%*#&:;.,_¡¿(<=>)?!]{8,}$";
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(password).matches();
    }

    public String getEncryptedKey(String key){
        String result = null;
        EncryptDecrypt encryptDecrypt = new EncryptDecrypt();
        try {
            SecretKey secretKey = encryptDecrypt.generateKey(getKey());
            byte[] encrypted =  encryptDecrypt.encrypt(secretKey, key);
            result = Base64.encodeToString(encrypted, Base64.DEFAULT);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidParameterSpecException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getDecryptedKey(String key){
        String result = null;
        EncryptDecrypt encryptDecrypt = new EncryptDecrypt();
        try {
            SecretKey secretKey = encryptDecrypt.generateKey(getKey());
            byte[] restore = Base64.decode(key, Base64.DEFAULT);
            result = encryptDecrypt.decrypt(secretKey, restore);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidParameterSpecException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void saveKey(byte[] key) {
        sharedStorage.setString(ConstantsAppgate.KEY, Base64.encodeToString(key, Base64.DEFAULT));
    }

    private byte[] getKey() {
        String strKey = sharedStorage.getString(ConstantsAppgate.KEY);
        if (strKey.isEmpty()) {
            EncryptDecrypt encryptDecrypt = new EncryptDecrypt();
            SecureRandom random = new SecureRandom();
            byte[] encryptionKey = new byte[16];
            random.nextBytes(encryptionKey);
            saveKey(encryptionKey);
            return encryptionKey;
        } else {
            return Base64.decode(strKey, Base64.DEFAULT);
        }
    }

    /**
     * get time service
     */

    private MutableLiveData<TimezoneAppgate> timeResponse = new MutableLiveData<>();
    private Observer<Resource<TimezoneAppgate>> timeObserver = timezoneAppgateResource -> {
        proccessTimeResponse(timezoneAppgateResource);
    };

    public void callGetTime(double lat, double lng) {
        repository.getTime(lat, lng).observeForever(timezoneAppgateResource -> {
            timeObserver.onChanged(timezoneAppgateResource);
        });
    }

    public LiveData<TimezoneAppgate> getTime() {
        return Transformations.map(timeResponse, input -> input);
    }

    private void proccessTimeResponse(Resource<TimezoneAppgate> response) {
        switch (response.status) {
            case SUCCESS:
                timeResponse.postValue(response.data);
                break;
            case ERROR:
                break;
            case LOADING :
                break;
        }
    }
}
