package com.challenge.appgate.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.challenge.appgate.R;
import com.challenge.appgate.databinding.ItemValidationBinding;
import com.challenge.data.model.UserValidation;

import java.util.List;

public class ValidationAdapter extends RecyclerView.Adapter<ValidationAdapter.MyViewHolder> {

    private List<UserValidation> validations;

    public ValidationAdapter(List<UserValidation> validations) {
        this.validations = validations;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemValidationBinding binding = DataBindingUtil.
                inflate(LayoutInflater.
                        from(parent.getContext()), R.layout.item_validation, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        UserValidation userValidation = validations.get(position);
        holder.bind(userValidation);
    }

    @Override
    public int getItemCount() {
        return validations.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ItemValidationBinding binding;

        MyViewHolder(ItemValidationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(UserValidation userValidation) {
            binding.setUserValidation(userValidation);
            binding.executePendingBindings();
        }
    }
}
