package com.challenge.appgate.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.Toast;

import com.challenge.appgate.R;
import com.challenge.appgate.databinding.ActivityValidationBinding;
import com.challenge.appgate.ui.adapter.ValidationAdapter;
import com.challenge.appgate.viewmodel.MainViewModel;
import com.challenge.appgate.viewmodel.ValidationViewModel;
import com.challenge.data.model.UserValidation;

import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ValidationActivity extends AppCompatActivity {

    public static final String ID_USER = "id_user";

     private ActivityValidationBinding binding;
     private ValidationViewModel viewModel;

     private ValidationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityValidationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        viewModel = new ViewModelProvider(this).get(ValidationViewModel.class);
        initView();
        initObservers();
    }

    private void initView() {
        int idUser = getIntent().getIntExtra(ID_USER, -1);
        viewModel.callGetUserValidations(idUser);
    }

    private void initObservers() {
        viewModel.getUserValidations().observe(this, userValidations -> {
            if (adapter == null){
                adapter = new ValidationAdapter(userValidations);
                binding.recycler.setAdapter(adapter);
            } else {
                adapter.notifyDataSetChanged();
            }
            Toast.makeText(this, "Total: " + userValidations.size(), Toast.LENGTH_SHORT).show();
        });
    }
}