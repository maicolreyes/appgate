package com.challenge.appgate.ui;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.challenge.appgate.databinding.ActivityMainBinding;
import com.challenge.appgate.viewmodel.MainViewModel;
import com.challenge.appgate.viewmodel.ValidationViewModel;
import com.challenge.core.encryptdecrypt.EncryptDecrypt;
import com.challenge.core.model.TimezoneAppgate;
import com.challenge.data.model.User;
import com.challenge.data.model.UserValidation;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 1989;
    private ActivityMainBinding binding;
    private MainViewModel viewModel;

    private FusedLocationProviderClient fusedLocationClient;

    private String currentTime = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        initViews();
        initClicks();
        initObservers();
    }

    private void initViews() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    private void getLocation() {
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {

            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        if (location != null) {
                            LiveData<Integer> isUser = viewModel.callIsUser(binding.editUsername.getText().toString());
                            isUser.observe(MainActivity.this, integer -> {
                                isUser.removeObservers(MainActivity.this);
                                if (integer > 0) {
                                    viewModel.callGetTime(location.getLatitude(), location.getLongitude());
                                } else {
                                    Toast.makeText(MainActivity.this, "User doesn't exists", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
            return;

        } else {
            // You can directly ask for the permission.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                        new String[] { Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION },
                        REQUEST_CODE);
            }
        }
    }

    private void initClicks() {
        binding.btnRegister.setOnClickListener(view -> {
            if (viewModel.isPassword(binding.editPassword.getText().toString())) {
                LiveData<Integer> isUser = viewModel.callIsUser(binding.editUsername.getText().toString());
                isUser.observe(this, integer -> {
                    isUser.removeObservers(MainActivity.this);
                    if (integer == 0) {
                        User user = new User(binding.editUsername.getText().toString(),
                                viewModel.getEncryptedKey(binding.editPassword.getText().toString()));
                        viewModel.saveUser(user);
                    } else {
                        Toast.makeText(MainActivity.this, "User already exists", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                Toast.makeText(this, "Invalid", Toast.LENGTH_SHORT).show();
            }
        });

        binding.btnValidate.setOnClickListener(view -> {
            getLocation();
        });
    }

    private void initObservers() {
        viewModel.getTime().observe(this, timezoneAppgate -> {
            currentTime = timezoneAppgate.getTime();
            viewModel.callGetUser(binding.editUsername.getText().toString());
        });

        viewModel.getUser().observe(this, user -> {
            String decryptedKey = viewModel.getDecryptedKey(user.getKey());
            if (binding.editPassword.getText().toString().compareTo(decryptedKey) == 0) {
                UserValidation userValidation = new UserValidation(user.getId(), true, currentTime);
                viewModel.saveUserValidation(userValidation);
                Intent intent = new Intent(this, ValidationActivity.class);
                intent.putExtra(ValidationActivity.ID_USER, user.getId());
                startActivity(intent);
            } else {
                Toast.makeText(this, "User not valid", Toast.LENGTH_SHORT).show();
                UserValidation userValidation = new UserValidation(user.getId(), false, currentTime);
                viewModel.saveUserValidation(userValidation);
            }
        });
    }

    private ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {

                } else {
                    //Show to the user why you need to access to location
                }
            });
}